# scripts

## dotenv.sh

A simple `.env` file loader that utilizes shell capabilities.

### Usage:

.env file:
```.env
HELLO=Hello
HELLO_WORLD=$HELLO World!
MESSAGE=$(printf "Message: %s" "$HELLO_WORLD")
```

Shell:

```sh
source dotenv.sh

echo "$MESSAGE" # Message: Hello World!
```

## ngx-cloudflare-restore.sh

Generates an nginx.conf include file to restore the client address after Cloudflare.

### Usage:

```sh
./ngx-cloudflare-resolve.sh -h # Help

./ngx-cloudflare-restore.sh -o /etc/nginx/conf.d/cloudflare.conf
```

## cloudflare-iptables.sh

Generates iptables commands or netfilter rules to whitelist Cloudflare.

### Usage:

```sh
./cloudflare-iptables.sh -h # Help

./cloudflare-iptables.sh -i eth0 -p 443 | sh -
./cloudflare-iptables.sh -i eth0 -p 443 -6 | sh -
# Or
./cloudflare-iptables.sh -i eth0 -p 443 -n >> /etc/iptables/rules.v4
./cloudflare-iptables.sh -i eth0 -p 443 -6 -n >> /etc/iptables/rules.v6
```
